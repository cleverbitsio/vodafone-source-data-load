/** The csv file needs to follow these two rules:
==============================================
1 - table column names need to be lower case!!
2 - No spaces between commas for the header!!
*/

CREATE KEYSPACE IF NOT EXISTS vodafone WITH REPLICATION = {'class':'SimpleStrategy', 'replication_factor':2} ;
USE vodafone; 
CREATE TABLE IF NOT EXISTS vodafone.prod(
 r_p_msisdn_code int,
 r_p_msc text,
 r_p_imsi_code int,
 zpcode int,
 actual_volume int,
 r_p_imei text,
 call_dest text,
 credit_clicks int,
 o_p_number text,
 ttcode int,
 r_p_ms_class_mark text,
 start_d_t timestamp,
 twcode int,
 rated_clicks int,
 sp_code int,
 r_p_sim_id int,
 facility_usage text,
 aoc_amount int,
 tzcode int,
 r_p_cgi text,
 r_p_np text,
 action_code text,
 tm_version int,
 umcode int,
 rated_volume int,
 rtx_lfnr int,
 sncode int,
 trac_start_d_t text,
 rtx_type text,
 original_start_d_t timestamp,
 rtx_sqn int,
 r_p_ton text,
 gvcode int,
 o_p_cgi text,
 termination_ind int,
 r_p_mccode int,
 rtx_sub_lfnr int,
 r_p_customer_id int,
 r_p_mcind int,
 bc int,
 call_type int,
 rated_flat_amount int,
 bill_status text,
 aoc_ind text,
 gsm_pi text,
 market_id text,
 plcode int,
 eccode int,
 roamer_ind text,
 rounded_volume int,
 delete_after_bill text,
 tmcode int,
 r_p_imsi int,
 vpn_ind text,
 tariff_class text,
 call_reference int,
 cause_for_term text,
 pstn_id int,
 routing_category text,
 rated_flat_amount_euro double,
 cdr_indicator text,
 r_p_number text,
 visited_cc_code int,
 data_volume int,
 o_p_rgn int,
 roam_rerating_ind int,
 inputed_to_primary text,
 primary_object_id int,
 origin_zpcode int,
 dialled_digits text,
 circe_product text,
 counter text,
 bonus text,
 wisp text,
 hot_spot text,
 authentication_type text,
 charging_id int,
 ggsn_address text,
 bearer int,
 bit_rate int,
 qosnegotiated text,
 promo_elab_date timestamp,
 primary_gn_sim_id int,
 connected_sim_id_from_gn int,
 number_pm_pulses int,
 apn_ind int,
 delay_call text,
 rated_flat_amount_euro_tm double,
 primary key ((r_p_sim_id),start_d_t)
);

/*
select count(*) from vodafone.prod;
truncate vodafone.prod;
select count(*) from vodafone.prod;

select * from vodafone.prod where r_p_sim_id='       183005745 ';
select * from vodafone.prod where r_p_sim_id='183005745';
select * from vodafone.prod where r_p_sim_id=183005745;
select * from vodafone.prod where r_p_sim_id=159610000;
select * from vodafone.prod where r_p_sim_id=183005745 and start_d_t>'2017-03-23 02:06:39+0000';
*/

